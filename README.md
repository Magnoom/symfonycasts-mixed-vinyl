# 1. Harmonious Development with Symfony 6
### 1. Plugins For PhpStorm:
- Symfony Support (Enable for each project)

### 2. Add templating library - Twig
- Type in terminal ```composer require templates``` or (```composer require symfony/twig-pack```)

### 3. Add Symfony debug panel to project
- Type in terminal ```composer require debug```

### 4. Add CSS styles
- First of all we need to add ```asset()``` support to Twig. Type in terminal ```composer require symfony/asset```
 
### 5. Install Encore
- Type in terminal ```composer require encore``` or (```composer require symfony/webpack-encore-bundle```).
- If you want to use Encore version 2 or higher, be sure to also run: ```composer require symfony/stimulus-bundle```.
The recipe needed to integrate the Symfony UX libraries was moved to this new bundle.

### 6. Add Bootstrap.css FontAwesome and Font Roboto-condensed like npm packages
- Bootstrap: ```npm add bootstrap --include=dev```. And import it on top of /assets/styles/app.css ```@import "~bootstrap";```.
- Font Roboto-Condensed: ```npm add @fontsource/roboto-condensed --include=dev```. And import it on top of /assets/styles/app.css ```@import "~@fontsource/roboto-condensed";```.
- Font Awesome: ```npm add @fortawesome/fontawesome-free --include=dev```.  And import it on top of /assets/styles/app.css ```@import "~@fortawesome/fontawesome-free/css/all.css";```.
 For some reason we need to include full path to CSS file in import string.

### 7. Add Axios for making http requests from JS
- ```npm add axios --include=dev```.

### 8. Adding Symfony UX Turbo
- ```composer require symfony/ux-turbo```. Experience with Single Page Application.

## Useful NPM commands
- ```npm install``` - installing all the packages from package-lock.json. Like ```composer install``` in PHP.
- ```npm add <package/name> [--include=dev]``` - add a package to the project [like dev package].
- ```npm run build``` - build the .js and .css files. Needed to see changes in browser.
- ```npm run watch``` - run the "watch" package script (watching for updates in .js and .css files and automatically applies for browser). So you don't need to make ```npm run build```
  !!! It's occupy the terminal window.

# 2. Symfony 6 Fundamentals Services, Config & Environments

### 1. Add KNP Labs time-bundle to make 'ago' dates
- ```composer require knplabs/knp-time-bundle```

### 2. Add HTTP Client for making HTTP Requests. Vinyl mixes json
- ```composer require symfony/http-client```
- While we don't have a DB yet, there is a Vinyl mixes JSON in GitHub (https://raw.githubusercontent.com/SymfonyCasts/vinyl-mixes/main/mixes.json)

### 3. Add cache for HTTP Response
- Use the CacheInterface from ```Symfony/Contracts/Cache```.
- Useful commands: ```php bin/console cache:pool:list``` - to see what cache you have. And ```php bin/console cache:pool:clear cache.app``` to clear Application cache.
- For clearing cache on prod when deploy ```php bin/console cache:clear```.
- Also, you can warm up the cache ```php bin/console cache:warmup```.
```php
$mixes = $cache->get('mixes_data', function(CacheItemInterface $cacheItem) use ($httpClient) {
    $cacheItem->expiresAfter(5);
    $response = $httpClient->request('GET', 'https://raw.githubusercontent.com/SymfonyCasts/vinyl-mixes/main/mixes.json');
    return $response->toArray();
});
```

### 4. Making own service
- Symfony automatically made class ```src/Service/MixRepository``` as a service. See ```php bin/console debug:autowiring --all```
The ```--all``` flag means that command will return Symfony's and OURS autowire services.

### 5. Getting parameters from .yaml
- In ```config/services.yaml``` add the code: 
```yaml
services:
    App\Service\MixRepository:
        bind:
            $isDebug: '%kernel.debug%'
```
- In ```MixRepository.php``` add in constructor this parameter.
```php
public function __construct(
    ...
    ...
    private bool $isDebug
)
{}
```

### 6. Sensitive info like HTTP Tokens
- Leave them in ```.env``` file if they are not sensitive. This file commiting to repo
- Leave them in ```.env.local``` if they are sensitive. Make this file also on prod and paste the data.
- ```.env.local``` in ```.gitignore``` file. So it doesn't committed to repo.
- If there are exist same env variables in the OS. The OS ones will rewrite that in the file.
- Command to see what env vars are set in ```.env``` files: ```php bin/console debug:dotenv```

### 7. Symfony's secret vault
- At first, you need a Sodium PHP Extension enabled. (Go to php.ini)
- Then, use it for example to storing GitHub Token: ```php bin/console secrets:set GITHUB_TOKEN``` - then type or paste the value.
- Secrets variables are overriding by .env files. So you need to remove these variables from .env files to be able to use them from secret vault.
- To see the list of secrets type ```php bin/console secrets:list```
- To reveal the values of secrets keys type ```php bin/console sectets:list --reveal```. For different (prod) environments add ```--env=prod``` parameter.

### 8. Add Maker Bundle (Code generation utility)
- We need Maker Bundle only for dev purposes, so, type this: ```composer require maker --dev```.
- Make new console command: ```php bin/console make:command```. Type the name with prefix e.g. ```app:talk-to-me```. It generates the file ```src/Command/TalkToMeCommand.php```.
- Execute this command: ```php bin/console app:talk-to-me```. With parameters: ```php bin/console app:talk-to-me YourName --yell```.

# 3. Doctrine, Symfony 6 & the Database
- Add Doctrine to the project: ```composer require doctrine```.
- Docker configuration from recipes - yes permanently.
- If you want to change the DB from Postgres to MySql or MariaDB - delete ```compose.yaml``` and ```compose.override.yaml``` files and run ```php bin/console make:docker:database```

### 1. Run docker with DB service
- Next:
   - A) Run ```docker-compose up -d database``` to start your database container or ```docker-compose up -d``` to start all of them.
   - B) If you are using the Symfony Binary, it will detect the new service automatically. Run ```symfony var:export --multiline``` to see the environment variables the binary is exposing. These will override any values you have in your ```.env``` files.
   - C) Run ```docker-compose stop``` will stop all the containers in ```compose.yaml```. ```docker-compose down``` will stop and destroy the containers.
   - Port ```3306``` will be exposed to a random port on your host machine.
- Run ```docker-compose ps``` to see all running containers.
- To see all the env. variables that used by Symfony type: ```symfony var:export --multiline```.
- To see all the commands that Symfony can do type: ```php bin/console``` or ```symfony console```.
- To create the database: ```php bin/console doctrine:database:create``` or ```symfony console doctrine:database:create```.

### 2. Entity Class
- for making Entity via console type: ```php bin/console make:entity``` and follow the instructions.

### 3. Migrations
- To apply changes in Entities to the database we need a migration. Type ```php bin/console make:migration``` if you set up the correct DataBaseUrl if not? type ```symfony console make:migration``` and Symfony took env. variables from docker.
- To execute the migrations type ```php bin/console doctrine:migrations:migrate``` if you set up correct DataBaseUrl. If not, type ```symfony console doctrine:migrations:migrate``` And Symfony took env. variables from docker.
- Woo! You can make SQL queries directly from the terminal by typing e.g. ```symfony console doctrine:query:sql 'SELECT * FROM vinyl_mix'```
- To see migrations status type ```symfony console doctrine:migrations:status```.
- You can visualize the migrations table by running ```symfony console doctrine:migrations:list```.

### 4. Adding new property to VinylMix Entity Class from command line
- Type ```php bin/console make:entity```. Type entity Class to update ```VinylMix```. Symfony detect that VinylMix Entity already exists and will just add a new property to it.
- After adding new property make and execute migration: ```symfony console make:migration```, ```symfony console doctrine:migrations:migrate```.
- To figure out you need migration or not after your changes type ```symfony console doctrine:schema:update --dump-sql```

### 5. Persisting to the DataBase
- Autowire ```EntityManagerInterface $entityManager``` to controller method. ```use Doctrine\ORM\EntityManagerInterface;```
- ```$entityManager->persist($object)``` - make Doctrine aware of the entity object. No talking to DataBase
- ```$entityManager->flush()``` - looking at all the entity objects that it is "aware of" and make whatever query is necessary to "save (insert, update)" that object to the database.

### 6. Querying the DataBase
- Code example:
```php
public function browse(EntityManagerInterface $entityManager, string $slug = null): Response
{
    $mixRepository = $entityManager->getRepository(VinylMix::class);
    $mixes = $mixRepository->findAll();
    dd($mixes);
}
```
- EntityManager knows about ```VinylMixRepository``` coz we specify ```VinylMix::class```. See the attribute above this class:
```php
#[ORM\Entity(repositoryClass: VinylMixRepository::class)]
class VinylMix
{
...
}
```
- ```VinylMixRepository``` is the service in the container. So we can autowire it directly:
```php
 #[Route('/browse/{slug}', name: 'app_browse')]
public function browse(VinylMixRepository $mixRepository, string $slug = null): Response
{
    $genre = $slug ? u(str_replace('-', ' ', $slug))->title(true) : null;

    $mixes = $mixRepository->findAll();

    return $this->render('vinyl/browse.html.twig', [
        'genre' => $genre,
        'mixes' => $mixes,
    ]);
}
```
- The takeaway is this: if you want to query from a table, you'll do that through the repository of the entity whose data you need.

### 7. Custom Entity Methods & Twig Magic
- To see where a class was used after deleting it type: ```git grep <name>``` e.g. ```git grep MixRepository```

### 8. The Query Builder
- The best way to make a custom query, is to create a new method in the repository for whatever entity you're fetching data for.
- To execute the query call ```->getQuery()``` (that turns it into a Query object) and then ```->getResult()```.
- ```getResult()``` - returns an array of the matching objects. ```getOneOrNullResult()``` - for one specific row or null.
- To keep life simple, choose an alias for an entity and consistently use it everywhere e.g. ```mix```.
```php
$queryBuilder = $queryBuilder ?? $this->createQueryBuilder('mix');
```

### 9. Querying for a Single Entity for a "Show" Page
```php
#[Route('/mix/{id}', name: 'app_mix_show')]
    public function show($id, VinylMixRepository $mixRepository): Response
    {
        $mix = $mixRepository->find($id);

        return $this->render('mix/show.html.twig', [
            'mix' => $mix,
        ]);
    }
```

### 10. Param Converter & 404's
- See Symfony docs for how to customize error pages.
- Autowire Entity in controller method.
```php
#[Route('/mix/{id}', name: 'app_mix_show')]
    public function show(VinylMix $mix, VinylMixRepository $mixRepository): Response
    {
        return $this->render('mix/show.html.twig', [
            'mix' => $mix,
        ]);
    }
```
In this case the wildcard argument should match the Entity Property. And this automatically generate 404 if no records found.
If you need to query for multiple objects, like in the browse() action, you'll use the correct repository service. But if you need to query for a single object in a controller, use this trick.

### 11. The Request Object and form submitting
- To getting requests from forms in controller you can autowire ```RequestStack``` service:
```php
#[Route('/mix/{id}/vote', name: 'app_mix_vote', methods: ['POST'])]
public function vote(VinylMix $mix, RequestStack $requestStack): Response
{
    $requestStack->getCurrentRequest();
    dd($mix);
}
```
This works anywhere when you autowire ```RequestStack``` service.
But in controllers we have an easier way:
```php
#[Route('/mix/{id}/vote', name: 'app_mix_vote', methods: ['POST'])]
public function vote(VinylMix $mix, Request $request): Response
{
    // 'direction' - is the name of field, or button etc. on the submitted form.
    // 'up' - is the default value if this key will not exist in the submitted form.
    $direction = $request->request->get('direction', 'up');
    ...
    dd($mix);
}
```
You can have four different types of arguments on a controller method.
- One: you can have route wildcards like $id.
- Two: You can autowire services.
- Three: You can type-hint entities.
- And four: You can type-hint the Request class.
Yup, the Request object is so important that Symfony created a special case just for it.

### 12. Update an Entity
```php
#[Route('/mix/{id}/vote', name: 'app_mix_vote', methods: ['POST'])]
public function vote(VinylMix $mix, Request $request, EntityManagerInterface $entityManager): Response
{
    $direction = $request->request->get('direction', 'up');
    if ($direction === 'up') {
        $mix->setVotes($mix->getVotes() + 1);
    } else {
        $mix->setVotes($mix->getVotes() - 1);
    }

//        $entityManager->persist($mix); // this line is redundant. But allowed.
    $entityManager->flush();
    return $this->redirectToRoute('app_mix_show', ['id' => $mix->getId()]);
}
```
- In this case the line ```$entityManager->persist($mix);``` is totally redundant because the Doctrine already knows about ```$mix``` object. Because we have it in method's arguments.

### 13. Flash Message & Rich vs Anemic Models
- To show the Flash message trigger int in controller method. In out case after ```flush()```.
```php
#[Route('/mix/{id}/vote', name: 'app_mix_vote', methods: ['POST'])]
public function vote(VinylMix $mix, Request $request, EntityManagerInterface $entityManager): Response
{
    ...
    $entityManager->flush();

    $this->addFlash('success', 'Vote counted!');

    return $this->redirectToRoute('app_mix_show', ['id' => $mix->getId()]);
}
```
- To show Flash Messages to user you need to add them to ```.twig``` template. In our case in ```base.html.twig```:
```html
{% for message in app.flashes('success') %}
    <div class="alert alert-success">
        {{ message }}
    </div>
{% endfor %}

{% block body %}{% endblock %}
```
- So, Rich models have not only getters and setters for table columns, but methods that fit project business logic like ```public function upVote(): void``` and ```public function downVote(): void``` in our case.

### 14. Doctrine Extensions: Timestampable (Sluggable, Loggable, etc.).
- UpdatedAt table column. The library that does it automatically: ```composer require stof/doctrine-extensions-bundle```. https://github.com/doctrine-extensions/DoctrineExtensions
- Enable Timestampable behaviour in config file: ```config/packages/stof_doctrine_extensions.yaml```
```yaml
stof_doctrine_extensions:
    default_locale: en_US
    orm:
        default:
            timestampable: true
```
- To use this functionality use the Trait ```TimestampableEntity``` in Entity class:
```php
#[ORM\Entity(repositoryClass: VinylMixRepository::class)]
class VinylMix
{
    use TimestampableEntity;
    ...
}
```
And then remove the ```$createdAt``` property from Entity and their getters and setters. As we have them for free from the Trait ```TimestampableEntity```.
- Then we need to make and run migration because we don't have ```updatedAt``` field in our table.
```symfony console make:migration``` and ```
- Running migration may cause an error if you have some daa in this table. Because we already have records in table and ```updatedAt``` column will have null value. But migration not allowing null.
So, drop DB ```symfony console doctrine:database:drop --force``` (or tweak migration to give the column a default value for existing records).
- Recreate the DB ```symfony console doctrine:database:create```.
- Rerun all the migrations ```symfony console doctrine:migrations:migrate```.
- !!! if you have an error ```Gedmo\Timestampable\Mapping\Event\Adapter\ORM::getRawData Value(): Argument #1 ($mapping) must be of type array, Doctrine\ORM\Mapping\FieldMapping given``` downgrade the ```doctrine/orm``` package to version ```^2.18```.
  - change version in ```composer.json``` and run ```composer update``` in terminal.

### 15. Clean URLs with Sluggable
- Enable sluggable in config: ```config/packages/stof_doctrine_extensions.yaml```
```yaml
stof_doctrine_extensions:
    default_locale: en_US
    orm:
        default:
            timestampable: true
            sluggable: true
```
- Next you need to add ```$slug``` property to the Entity:
  - Run ```php bin/console make:entity```.
  - Type the name of the existing Entity (e.g. ```VinylMix``` in our case)
  - Add ```slug```, type ```string```, length ```100```, nullable ```no```.
- Generate and run new migration for new property
  - Run ```symfony console make:migration```.
  - Run ```symfony console doctrine:migrations:migrate```.
- If you have records in this table, drop and generate the DB. Rerun migrations.
  - Run ```symfony console doctrine:database:drop --force```.
  - Run ```symfony console doctrine:database:create```.
  - Run ```symfony console doctrine:migrations:migrate```.

### 16. Simple Doctrine Data Fixtures.
- Run ```composer require --dev orm-fixtures```
- To load fixtures to the DataBase run ```symfony console doctrine:fixtures:load```

### 17. Foundry: Fixtures You'll Love (Library that easily generates fake data for fixtures)
- Install this library for developing: ```composer require zenstruck/foundry --dev```
- For each Entity class you need to have a corresponding Factory (Fixtures) Class. Let's make for ```VinylMix``` Entity: ```php bin/console make:factory```.

### 18. Pagination using PagerFanta library
- You can use built in Doctrine ability to paginate, but now we will be using another library: ```composer require babdev/pagerfanta-bundle pagerfanta/doctrine-orm-adapter```.
- To add pagination to page, we need a library from PagerFanta for twig:```composer require pagerfanta/twig```
- Add new config file ```config/packages/babdev_pagerfanta.yaml```:
```yaml
babdev_pagerfanta:
  default_view: twig
  default_twig_template: '@BabDevPagerfanta/twitter_bootstrap5.html.twig'
```
- Add pagination to template file ```templates/vinyl/browse.html.twig```
```html
        ......
        </div>
    {% endfor %}

    {{ pagerfanta(pager) }}
</div>
```
- If the changes didn't apply, try to reload cache ```php bin/console cache:clear``` and rebuild npm if it isn't watching ```npm run build```.

### 19. Forever scroll (for pagination) with Turbo Frames.
- So, this ```<turbo-frame>``` will start empty... but as soon as we scroll down to it, Turbo will make an AJAX request for the next page of results.
  - For example, if this frame is loading for page 2, the Ajax response will contain a ```<turbo-frame>``` with ```id="mix-browse-list-2"```. The Turbo Frame system will grab that from the Ajax response and put it here at the bottom of our list. And if there's a page 3, that will include yet another Turbo Frame down here that will point to Page 3.